# !/usr/bin/python
# -*-coding: utf-8-*-

#Paquetes requeridos
from flask import Flask
from flask import render_template
from flask import request
import os
import sys
import string
import psycopg2

app = Flask(__name__)

#/templates/index.html
#Primera plana al ejecutar el programa.
@app.route('/')#Especifica la ruta desde dónde se llama la función.
def index():
    return render_template('index.html')
    
#BOTONES PRINCIPALES

#/templates/consultar.html    
#Esta acción devuelve la página html consultar, se da cuando desde nuestro index.html (página principal) nos hacen la petición (/consultar) definida en el index.html mediante un botón.
@app.route('/consultar')
def consultar ():
    return render_template('consultar.html')

#/templates/anyadir.html
#Esta acción devuelve la página html anyadir, se da cuando desde nuestro index.html (página principal) nos hacen la petición (/anyadir) definida en el index.html mediante un botón.
@app.route('/anyadir')
def anyadir ():
    
    try:#Intenta conectarse a la base de datos usando la librería psycopg2, en nuestro caso llamada "proyecto" autenticarse con usuario y contraseña.
        conn = psycopg2.connect(database="proyecto", user="postgres", password="jupiter")
        print "DATABASE OPENED SUCCESSFULLY \n"#Si funciona correctamente nos avisará mediante la terminal del servidor.
        
    except:
        print "CONNECTION ERROR"#Si no funciona nos avisará mediante la terminal del servidor y saldrá.
        exit(2)
    
    #De la conexión hecha anteriormente, con cursor mira todas las líneas, las recorre, esto quiere decir que puede ir arriba y abajo. Las añade a la variable "cur".
    cur = conn.cursor()
    
    try:
        
        query1='select * from tipos;'
        query2='select id_fab, descripcion from fabricantes;'
        
        cur.execute(query1);
        rows = cur.fetchall()
        
        cur.execute(query2);
        rows2 = cur.fetchall()
        
    except psycopg2.Error as er :#En el caso que haya cualquier error nos notificará con el siguiente print.
        print "-------- ERROR:", er.pgcode, " -------- \n"
    
    
    conn.close()
    return render_template('anyadir.html', rows=rows, rows2=rows2)

#/templates/borrar.html    
#Esta acción devuelve la página html borrar, se da cuando desde nuestro index.html (página principal) nos hacen la petición (/borrar) definida en el index.html mediante un botón.
@app.route('/borrar')
def borrar ():
    return render_template('borrar.html')
    
#/templates/modificar.html    
#Esta acción devuelve la página html modificar, se da cuando desde nuestro index.html (página principal) nos hacen la petición (/modificar) definida en el index.html mediante un botón.
@app.route('/modificar')
def modificar ():
    return render_template('modificar.html')
    
#PETICIONES DE LOS HTML

@app.route('/selector')
def selector ():
    
    try:
        conn = psycopg2.connect(database="proyecto", user="postgres", password="jupiter")
        print "DATABASE OPENED SUCCESSFULLY \n"
        
    except:
        print "CONNECTION ERROR"
        exit(2)
    
    cur = conn.cursor()
    
    try:    
        
        #Recogemos las variables necesarias enviadas desde el formulario html con "request.args.get" y las definimos en otras variables de python (esta acción se realiza en todas las funciones).
        tablas=request.args.get('tablas')
        columna1=request.args.get('columna1')
        columna2=request.args.get('columna2')
        columna3=request.args.get('columna3')
        columna4=request.args.get('columna4')
        columna5=request.args.get('columna5')
        columna6=request.args.get('columna6')
        columna7=request.args.get('columna7')
        columna8=request.args.get('columna8')
        columna9=request.args.get('columna9')
        columna10=request.args.get('columna10')
        todo=request.args.get('todo')
        todo2=request.args.get('todo2')
        todo3=request.args.get('todo3')
        columna=""
        

        if tablas == "tipos": #Ahora después de que nos hayan hecho una petición desde consultar.html comprobaremos que tabla ha sido seleccionada mediante un selector en el formulario.
            
            #Dentro de los "ifs" iniciales tendremos otros los cuales nos harán una comprobación de si las variables que nos vienen del formulario que está en consultar.html están vacías o no y nos concatenará las que esten marcadas y se insertará todo dentro de la variable "columna". Esto lo utilizaremos luego para poder conformar el "select".
        
            if todo == None:
                if columna2 == None:
                    if columna1 == None:
                        pass
                    else:
                        columna = columna1
                else:
                    if columna1 == None:
                        columna = columna2
                    else:
                        columna = columna1 + ', ' + columna2
            else:
                columna = todo
                
        if tablas == "fabricantes":
        
            #La comprobación de las variables de la tabla fabricantes:
        
            if todo3 == None:
                if columna10 == None:
                    if columna9 == None:
                        if columna8 == None:
                            pass
                        else:
                            columna = columna8
                    else:
                        if columna8 == None:
                            columna = columna9
                        else:
                            columna = columna8 + ', ' + columna9
                else:
                    if columna9 == None:
                        if columna8 == None:
                            columna = columna10
                        else:
                            columna = columna8 + ', ' + columna10
                    else:
                        if columna8 == None:
                            columna = columna9 + ', ' + columna10
                        else:
                            columna = columna8 + ', ' + columna9 + ', ' + columna10
            else:
                columna = todo3
        
        if tablas == "productos":
            
            #La comprobación de las variables de la tabla productos:
        
            if todo2 == None:
                if columna7 == None:
                    if columna6 == None:
                        if columna5 == None:
                            if columna4 == None:
                                if columna3 == None:
                                    pass
                                else:
                                    columna = columna3
                            else:
                                if columna3 == None:
                                    columna = columna4
                                else:
                                    columna = columna3 + ', ' + columna4
                        else:
                            if columna4 == None:
                                if columna3 == None:
                                    columna = columna5
                                else:
                                    columna = columna3 + ', ' + columna5
                            else:
                                if columna3 == None:
                                    columna = columna4 + ', ' + columna5
                                else:
                                    columna = columna3 + ', ' + columna4 + ', ' + columna5
                    else:
                        if columna5 == None:
                            if columna4 == None:
                                if columna3 == None:
                                    columna = columna6
                                else:
                                    columna = columna3 + ', ' + columna6
                            else:
                                if columna3 == None:
                                    columna = columna4 + ', ' + columna6
                                else:
                                    columna = columna3 + ', ' + columna4 + ', ' + columna6
                        else:
                            if columna4 == None:
                                if columna3 == None:
                                    columna = columna5 + ', ' + columna6
                                else:
                                    columna = columna3 + ', ' + columna5 + ', ' + columna6
                            else:
                                if columna3 == None:
                                    columna = columna4 + ', ' + columna5 + ', ' + columna6
                                else:
                                    columna = columna3 + ', ' + columna4 + ', ' + columna5 + ', ' + columna6
                else:
                    if columna6 == None:
                        if columna5 == None:
                            if columna4 == None:
                                if columna3 == None:
                                    columna = columna7
                                else:
                                    columna = columna3 + ', ' + columna7
                            else:
                                if columna3 == None:
                                    columna = columna4 + ', ' + columna7
                                else:
                                    columna = columna3 + ', ' + columna4 + ', ' + columna7
                        else:
                            if columna4 == None:
                                if columna3 == None:
                                    columna = columna5 + ', ' + columna7
                                else:
                                    columna = columna3 + ', ' + columna5 + ', ' + columna7
                            else:
                                if  columna3 == None:
                                    columna = columna4 + ', ' + columna5 + ', ' + columna7
                                else:
                                    columna = columna3 + ', ' + columna4 + ', ' + columna5 + ', ' + columna7
                    else:
                        if columna5 == None:
                            if columna4 == None:
                                if columna3 == None:
                                    columna = columna6 + ', ' + columna7
                                else:  
                                    columna = columna3 + ', ' + columna6 + ', ' + columna7
                            else:
                                if columna3 == None:
                                    columna = columna4 + ', ' + columna6 + ', ' + columna7
                                else:
                                    columna = columna3 + ', ' + columna4 + ', ' + columna6 + ', ' + columna7
                        else:
                            if columna4 == None:
                                if columna3 == None:
                                    columna = columna5 + ', ' + columna6 + ', ' + columna7
                                else:
                                    columna = columna3 + ', ' + columna5 + ', ' + columna6 + ', ' + columna7
                            else:
                                if columna3 == None:
                                    columna = columna4 + ', ' + columna5 + ', ' + columna6 + ', ' + columna7
                                else:
                                    columna = columna3 + ', ' + columna4 + ', ' + columna5 + ', ' + columna6 + ', ' + columna7
            else:
                columna = todo2
            
        query='select ' + columna + ' from ' + tablas + ';' #Aquí construiremos lo que será nuestro "select" gracias a las variables previamente recogidas y lo insertaremos en la variable "query".
        cur.execute(query); #Esto será lo que haga que se ejecute dentro de postgres nuestro "select" almacenado en la variable "query".
        rows = cur.fetchall() #Busca todo lo que ha salido en el select (el resultado) y lo almacena en la variable "rows".
        
        cabecera = columna.split(',') #En el html también necesitamos la cabecera de la tabla pero esta no se almacena con "cur.fetchall()" anteriormente explicado, por eso mismo necesitamos conformarla a partir de dividir por comas y almacenar el contenido de la variable "columnas" en la variable "cabecera".
        
        #Esto (justo lo anterior) solo en el caso de que la variable "columna" no sea igual a todo, todo2 o todo3, si "columna" es igual a lo anterior mencionado la variable "cabecera" se definirá de la siguiente manera.
        
        if tablas == "productos" and columna == todo2:
            
            cabecera = [u'id_mat', u' descripcion', u' precio', u' id_fab', u' id_tipo']
            
        if tablas == "fabricantes" and columna == todo3:
            
            cabecera = [u'id_fab', u' descripcion', u' fecha_alta']
            
        if tablas == "tipos" and columna == todo:
            
            cabecera = [u'id_tipo', u' tipo']
            
        numcab = len(cabecera) #Esta variable nos sacará el número de elementos que tiene la variable "cabecera" y es necesaria para que en consultar.html a la hora de crear la tabla con el resultado almacenado en la variable "rows" sepamos cuántas líneas de la tabla tendremos de crear.

    #Si el comando no va nos mostrará que error és:
    except psycopg2.Error as er :
        print "-------- ERROR:", er.pgcode, " -------- \n"
            
    conn.close()
    return render_template('consultar.html', rows=rows, cabecera=cabecera, numcab=numcab)

#Botón añadir
@app.route('/anyadidor')
def anyadidor ():
    
    try:
        conn = psycopg2.connect(database="proyecto", user="postgres", password="jupiter")
        print "DATABASE OPENED SUCCESSFULLY \n"
        
    except:
        print "CONNECTION ERROR"
        exit(2)
    
    cur = conn.cursor()
    
    try:
        
        tablas=request.args.get('tablas')
        columna1=request.args.get('columna1')
        columna2=request.args.get('columna2')
        columna3=request.args.get('columna3')
        columna4=request.args.get('columna4')
        columna5=request.args.get('columna5')
        columna6=request.args.get('columna6')
        columna7=request.args.get('columna7')
        columna8=request.args.get('columna8')
    
        #Comprobamos que tabla ha sido seleccionada en los siguientes "ifs" principales.
        if tablas == "tipos":
            campos= "'" + columna7 + "', " + "'" + columna8 + "'" #Variable "campos"
            tablas += "(id_tipo, tipo)" #Variable tablas
        
        if tablas == "fabricantes":
            campos= "'" + columna5 + "', " + "'" + columna6 + "'" #Variable "campos"
            tablas += "(descripcion, fecha_alta)" #Variable tablas
            print campos
            
        if tablas == "productos":
            campos= "'" + columna1 + "', " + "'" + columna2 + "', " + "'" + columna3 + "', " + "'" + columna4 + "'" #Variable "campos"
            tablas += "(descripcion, precio, id_fab, id_tipo)" #Variable tablas
            
        #En la variable "campos" se almacenará las variables que nos vienen desde el html y esta contendra la informacion que se insertará mediante un "insert" en su respectiva tabla y columna.
        #En la variable "tablas" se almacenará tanto la variable que nos viene desde el html como también le añadiremos dependiendo de qué tabla se trate los campos que se tendrán que rellenar con la variables "campos".
        
        query='insert into ' + tablas + ' values (' + campos + ');' # En la variable "query" tendremos el "insert" que se nos ejecutara en postgres conformado con las diferentes variables.
        
        #Lo siguiente nos sirve para conformar la información que aparecerá en los botones que tenemos para seleccionar id_tipo o id_fab que está en anyadir.html. El funcionamiento concreto se ha explicado previamente en el apartado de Conexión entre tablas, claves foráneas.
        
        query1='select * from tipos;'
        query2='select id_fab, descripcion from fabricantes;'
        
        cur.execute(query1);
        rows = cur.fetchall()
        
        cur.execute(query2);
        rows2 = cur.fetchall()
        
        error = False #Esta variable (error) se utiliza en anyadir.html para mostrar un error o  no dependiendo de el valor de esta. Si el valor es "False" no muestra nada y si es "True" nos muestra un "alert" en la página html.
        cur.execute(query);
        conn.commit(); #Esto es importante para que se nos guarden los cambios que realicemos durante la conexión.
        
    except psycopg2.Error as er :
        print "-------- ERROR:", er.pgcode, " -------- \n"
        error = True #Variable error
    
    conn.close()
    return render_template('anyadir.html', rows=rows, rows2=rows2, error=error)
    
@app.route('/borrador')
def borrador ():
    
    try:
        conn = psycopg2.connect(database="proyecto", user="postgres", password="jupiter")
        print "DATABASE OPENED SUCCESSFULLY \n"
        
    except:
        print "CONNECTION ERROR"
        exit(2)
    
    cur = conn.cursor()
    
    try:
        
        tablas=request.args.get('tablas')
        columna1=request.args.get('columna1')
        columna2=request.args.get('columna2')
        columna3=request.args.get('columna3')
        contenido1=request.args.get('contenido1')
        contenido2=request.args.get('contenido2')
        contenido3=request.args.get('contenido3')
        
        #Comprobamos que tabla ha sido seleccionada en los siguientes "ifs" principales.
        if tablas == "productos":
            query='delete from productos where ' + columna1 + '=' + "'" + contenido1 + "'" + ';' #Variable query
            query2= 'select * from productos where ' + columna1 + '=' + "'" + contenido1 + "'" #Variable query2
            cabecera = [u'id_mat', u' descripcion', u' precio', u' id_fab', u' id_tipo'] #Variable cabecera
            
        if tablas == "fabricantes":
            query='delete from fabricantes where ' + columna2 + '=' + "'" + contenido2 + "'" + ';' #Variable query
            query2= 'select * from fabricantes where ' + columna2 + '=' + "'" + contenido2 + "'" #Variable query2
            cabecera = [u'id_fab', u' descripcion', u' fecha_alta'] #Variable cabecera
        
        if tablas == "tipos":
            query='delete from tipos where ' + columna3 + '=' + "'" + contenido3 + "'" + ';' #Variable query
            query2= 'select * from tipos where ' + columna3 + '=' + "'" + contenido3 + "'" #Variable query2
            cabecera = [u'id_tipo', u' tipo'] #Variable cabecera
            
        #La variable "query" tiene el "delete" que se utilizara para borrar las líneas de la tabla pertinente. Está compuesta por las diferentes variables que vienen del html.
        #La variable "query2" contiene un "select" el cual nos sacara la o las líneas que se van a borrar con el "delete". Se usará para que el usuario visualize en forma de tabla las líneas que se han borrado.
        #La variable "cabecera" se usará como ya hemos explicado previamente para mostrar la cabecera de la tabla en el html.
        
        cur.execute(query2);
        rows = cur.fetchall()
        
        if len(rows) == 0: #Aquí comprobamos la longitud de la variable "rows" y si esta es igual a 0 significa que no se ha borrado nada por eso mismo le pondremos el valor de True a la variable "error".
            cabecera = "" #Le asignamos este valor a la variable "cabecera" para que no nos muestre nada por html.
            error = True #Variable error
        else:
            error = False #Variable error
        
        numcab = len(cabecera)
        cur.execute(query);
        conn.commit();
        
    except psycopg2.Error as er :
        print "-------- ERROR:", er.pgcode, " -------- \n"
        error = True #Variable error
    
    conn.close()
    return render_template('borrar.html', rows=rows, cabecera=cabecera, numcab=numcab, error=error)
    
@app.route('/modificador')
def modificador ():
    
    try:
        conn = psycopg2.connect(database="proyecto", user="postgres", password="jupiter")
        print "DATABASE OPENED SUCCESSFULLY \n"
        
    except:
        print "CONNECTION ERROR"
        exit(2)
    
    cur = conn.cursor()
    
    try:
        
        tablas=request.args.get('tablas')
        tablas_mod=request.args.get('tablas_mod')
        tablas2_mod=request.args.get('tablas2_mod')
        tablas3_mod=request.args.get('tablas3_mod')
        tablas4_mod=request.args.get('tablas4_mod')
        tablas5_mod=request.args.get('tablas5_mod')
        tablas6_mod=request.args.get('tablas6_mod')
        columna1=request.args.get('columna1')
        columna2=request.args.get('columna2')
        columna3=request.args.get('columna3')
        columna4=request.args.get('columna4')
        columna5=request.args.get('columna5')
        columna6=request.args.get('columna6')
    
        if tablas == "tipos":
            
            query='update '+ tablas + ' set ' + tablas6_mod + "=" + "'" + columna6 + "'" + ' where ' + tablas5_mod + '=' + "'" + columna5 + "'" + ';' #Variable query
            
        if tablas == "fabricantes":
            
            query='update '+ tablas + ' set ' + tablas4_mod + "=" + "'" + columna4 + "'" + ' where ' + tablas3_mod + '=' + "'" + columna3 + "'" + ';' #Variable query
            
        if tablas == "productos":
            
            query='update '+ tablas + ' set ' + tablas2_mod + "=" + "'" + columna2 + "'" + ' where ' + tablas_mod + '=' + "'" + columna1 + "'" + ';' #Variable query
            
        #La variable query tiene el "update" el cual utilizaremos para modificar ciertas casillas de nuestras tablas.
            
        cur.execute(query);
        conn.commit();
        error = False #Variable error
    
    except psycopg2.Error as er :
        print "-------- ERROR:", er.pgcode, " -------- \n"
        error = True #Variable error
    
    
    conn.close()
    return render_template('modificar.html', error=error)
        
if __name__ == '__main__':
    app.run(debug=True, port=5000, host="0.0.0.0")


