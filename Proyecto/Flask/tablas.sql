--Executar dintre de la base de dades--
------------Creació taules-------------

CREATE TABLE IF NOT EXISTS fabricantes (
id_fab serial PRIMARY KEY, --serial--
descripcion varchar(30) NOT NULL, --varchar(30)--
fecha_alta date --date--
);

CREATE TABLE IF NOT EXISTS tipos (
id_tipo char(4) PRIMARY KEY, --char(2)--
tipo varchar(20) NOT NULL
);

CREATE TABLE IF NOT EXISTS productos (
id_mat serial PRIMARY KEY,
descripcion varchar(50) NOT NULL, 
precio decimal NOT NULL,
id_fab int references fabricantes(id_fab) on delete cascade on update cascade, --int-- --references--
id_tipo varchar(4) references tipos(id_tipo) on delete cascade on update cascade --on delete/update cascade--
);

--serial: crea automáticamente las claves primárias--
--varchar(30): el número de caracteres puede variar, entre paréntesis se define el número máximo de caracteres-- 
--date: formato fecha--
--char(2): ha de contener el número de caracteres específicado--
--int: valor numérico-- 
--references: id_fab referencia al campo id_fab de la tabla fabricantes creada anteriormente, és una foreing key--
--on delete/update cascade: al borrar o actualizar estos campos, las foreign keys sufrirán el mismo efecto--

--CONTENIDO TABLA FABRICANTE--

insert into fabricantes(descripcion, fecha_alta) values ('Samsung', '2018-04-20');
insert into fabricantes(descripcion, fecha_alta) values ('Newskill', '2017-12-15');
insert into fabricantes(descripcion, fecha_alta) values ('Intel', '2018-01-19');
insert into fabricantes(descripcion, fecha_alta) values ('MSI', '2018-03-27');
insert into fabricantes(descripcion, fecha_alta) values ('Gigabyte', '2018-04-13');
insert into fabricantes(descripcion, fecha_alta) values ('Lenovo', '2017-10-06');

--CONTENIDO TABLA TIPOS--

insert into tipos(id_tipo, tipo) values ('KO', 'componente');
insert into tipos(id_tipo, tipo) values ('OR', 'ordenador');
insert into tipos(id_tipo, tipo) values ('PE', 'periferico');

--CONTENIDO TABLA PRODUCTOS--

insert into productos(descripcion, precio, id_fab, id_tipo) values ('Pen 4GB', 7, 1, 'PE');
insert into productos(descripcion, precio, id_fab, id_tipo) values ('Teclado Mecanico Newskill', 70, 2, 'PE');
insert into productos(descripcion, precio, id_fab, id_tipo) values ('Intel Core i7', 310, 3, 'KO');
insert into productos(descripcion, precio, id_fab, id_tipo) values ('Pen 4GB', 7, 1, 'PE');
insert into productos(descripcion, precio, id_fab, id_tipo) values ('PC MSI Gaming', 749, 4, 'OR');
insert into productos(descripcion, precio, id_fab, id_tipo) values ('Monitor Samsung', 100, 1, 'PE');
insert into productos(descripcion, precio, id_fab, id_tipo) values ('Raton RGB', 20, 4, 'PE');
insert into productos(descripcion, precio, id_fab, id_tipo) values ('Placa Base Gigabyte', 117, 5, 'KO');
insert into productos(descripcion, precio, id_fab, id_tipo) values ('Portatil Lenovo', 680, 6, 'OR');
insert into productos(descripcion, precio, id_fab, id_tipo) values ('GeForce GTX 1050', 208, 5, 'KO');
