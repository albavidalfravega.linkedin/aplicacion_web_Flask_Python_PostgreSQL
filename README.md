El proyecto consiste en crear una base de datos con PostgreSQL, mediante scripts realizamos la instalación inicial y la creación de las tablas.
La Aplicación Web está programada con Flask (código Python).

Desarrolladores: Alumnos de la Escuela del Trabajo, Alba Vidal y David Bautista.

### Herramientas necesarias.

1. Python (Flask)
2. PostgreSQL
3. HTML
4. CSS
5. Javascript
6. Drive

### Conceptos PostgreSQL.

Primeros pasos, instalación y servicios activos:

```
[root@a11 ~]# dnf install postgresql
[root@a11 ~]# dnf install postgresql-server
[root@a11 ~]# systemctl enable postgresql
[root@a11 ~]#postgresql-setup initdb
``` 
## Para acceder a Postgres:

```
[root@a11 ~]# passwd postgres
[root@a11 ~]# su postgres
Password:
bash-4.3$ psql
``` 
Comandos en psql:

    #Lista las bases de datos
    postgres=# \l 

    #Crear una base de datos
    postgres=# create database proyecto;
    CREATE DATABASE

    #Conectarse a una base de datos
    postgres=# \c proyecto
    You are now connected to database "proyecto" as user "postgres".
    #Lista las tablas existentes dentro de la base de datos a la que estamos conectados
    proyecto=# \d

    #Salir
    proyecto=# \q
    bash-4.3$ exit

    #Eliminar una base de datos
    postgres=# drop database proyecto;
    DROP DATABASE
    
### Acceder a Postgres desde Python.

Primeros pasos:

Instalamos el paquete psycopg2.

    [root@a11 ~]# dnf -y install python-psycopg2

Psycopg es el adaptador PostgreSQL para el lenguaje de programación Python. Con este podremos acceder a nuestra base de datos a través de los scripts hechos con py.

    [root@a11 ~]# geany /var/lib/pgsql/data/pg_hba.conf

    # "local" is for Unix domain socket connections only
    #local   all                          all                                   peer
    local    all                all                       trust

### Conceptos Flask.
## Para que funcione el servidor:
El flask servirá como servidor para acceder desde cualquier otra máquina como para poder verlo todo desde una interfaz web, así siendo visualmente amigable para el usuario final. 

Instalación del paquete Flask:

    [root@a11 ~]# pip install Flask

Si al ejecutar un script.py da error porque no encuentra el módulo flask, ejecutar:

    [root@a12]~# dnf install -y python-flask

Esto se debe a que flask se ha instalado en otra versión de python.

Creamos el script .python y en este importamos flask:

    # !/usr/bin/python
    # -*-coding: utf-8-*-
    #Paquetes requeridos
    from flask import Flask
    from flask import render_template
    from flask import request
    import os
    import sys
    import string
    import psycopg2

    app = Flask(__name__)

    @app.route('/')
    def index():
        return render_template('index.html')
    if __name__ == '__main__':
        app.run(debug=True, port=5000, host="0.0.0.0")

En app.run añadimos: “ host=”0.0.0.0” ”
Quedará así: app.run(debug=True, port=5000, host="0.0.0.0")
Debug=True lo que hace es captar los cambios hechos desde el servidor y no hace falta cerrar y volver a abrir el servidor cada vez que se efectúe un cambio.
Abrimos el puerto por el cual lo serviremos que en este caso es el 5000. 

Para que otra máquina pueda acceder es importante tener el puerto (en este caso 5000) abierto:

    [root@a11 ~]#firewall-cmd --permanent --add-port={5000/tcp,5000/udp}
    [root@a11 ~]#firewall-cmd --reload

Para que el cliente se pueda conectar lo único que ha de hacer es abrir un navegador y en url escribir la IP y el puerto:

    192.168.4.11:5000
    
Para más información acceda a la [documentación](https://gitlab.com/albavidalfravega.linkedin/aplicacion_web_Flask_Python_PostgreSQL/raw/master/Proyecto/PROYECTO.pdf)
